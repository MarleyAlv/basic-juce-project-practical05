/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    addAndMakeVisible(blobColour);
    addAndMakeVisible(testButton);
    addAndMakeVisible(testRow);
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    blobColour.setBounds (0, 0, getWidth(), getHeight()/2);
    testButton.setBounds (10, getHeight()/2, getWidth()/12.5, getHeight()/10);
    testRow.setBounds(0, getHeight()/1.6, getWidth(), (getHeight()+40)/5);
}
void MainComponent::paint (Graphics& g)
{
    g.setColour(blobColour.getCurrentColour());
    g.fillEllipse (x-(getWidth()/33.32), y-(getHeight()/26.66), getWidth()/16.66, getHeight()/13.33);
}
void MainComponent::mouseUp (const MouseEvent& event)
{
    x = event.x;
    y = event.y;
    
    DBG("Mouse up:(" << event.x << ", " << event.y << ")\n");
    repaint();
}