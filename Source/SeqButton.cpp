//
//  SeqButton.cpp
//  JuceBasicWindow
//
//  Created by Marley Alveranga on 22/11/2015.
//
//

#include "SeqButton.h"

SeqButton::SeqButton()
{
    colour = juce::Colours::silver;
}
SeqButton::~SeqButton()
{
    
}

void SeqButton::paint (Graphics& g)
{
    g.setColour(colour);
    g.fillEllipse(0, 0, getWidth(), getHeight());
}
void SeqButton::mouseEnter (const MouseEvent& event)
{
    colour = juce::Colours::gold;
    repaint();
}
void SeqButton::mouseExit (const MouseEvent& event)
{
    colour = juce::Colours::silver;
    repaint();
}
void SeqButton::mouseDown (const MouseEvent& event)
{
        colour = juce::Colours::goldenrod;
        repaint();
}
void SeqButton::mouseUp (const MouseEvent& event)
{
        colour = juce::Colours::gold;
        repaint();
}