//
//  ButtonRow.cpp
//  JuceBasicWindow
//
//  Created by Marley Alveranga on 23/11/2015.
//
//

#include "ButtonRow.h"

ButtonRow::ButtonRow()
{
    addAndMakeVisible(button1);
    addAndMakeVisible(button2);
    addAndMakeVisible(button3);
    addAndMakeVisible(button4);
    addAndMakeVisible(button5);
}
ButtonRow::~ButtonRow()
{
    
}

void ButtonRow::resized()
{
    int buttonWidth = (getWidth()-60)/5;
    
    button1.setBounds(10, 0, buttonWidth, getHeight());
    button2.setBounds(buttonWidth+20, 0, buttonWidth, getHeight());
    button3.setBounds((buttonWidth*2)+30, 0, buttonWidth, getHeight());
    button4.setBounds((buttonWidth*3)+40, 0, buttonWidth, getHeight());
    button5.setBounds((buttonWidth*4)+50, 0, buttonWidth, getHeight());
}