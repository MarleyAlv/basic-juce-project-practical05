//
//  ButtonRow.h
//  JuceBasicWindow
//
//  Created by Marley Alveranga on 23/11/2015.
//
//

#ifndef BUTTONROW_H_INCLUDED
#define BUTTONROW_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "SeqButton.h"

class ButtonRow   : public Component
{
public:
    //==============================================================================
    ButtonRow();
    ~ButtonRow();
    
    void resized() override;
private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ButtonRow)
    SeqButton button1;
    SeqButton button2;
    SeqButton button3;
    SeqButton button4;
    SeqButton button5;
};

#endif /* BUTTONROW_H_INCLUDED */
