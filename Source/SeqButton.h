//
//  SeqButton.h
//  JuceBasicWindow
//
//  Created by Marley Alveranga on 22/11/2015.
//
//

#ifndef SEQBUTTON_H_INCLUDED
#define SEQBUTTON_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class SeqButton   : public Component
{
public:
    //==============================================================================
    SeqButton();
    ~SeqButton();
    
    void paint (Graphics& g) override;
    void mouseEnter (const MouseEvent& event) override;
    void mouseExit (const MouseEvent& event) override;
    void mouseDown (const MouseEvent& event) override;
    void mouseUp (const MouseEvent& event) override;
    
private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SeqButton)
    Colour colour;
};

#endif /* SEQBUTTON_H_INCLUDED */
